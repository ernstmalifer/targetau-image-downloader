var sys = require('sys')
var exec = require('child_process').exec;
var request = require('request');
var http = require('http');
var fs = require('fs');
var async = require('async');
var cheerio = require('cheerio');

var itemsjson = __dirname + '/items.json';

fs.readFile(itemsjson, 'utf8', function (err, data) {

	items = JSON.parse(data).items;
	var i = 0;

	async.each(items, function(item, callback) {

		var options = {
			host: 'www.target.com.au',
			path: '/p/' + item.name
		};

		var timeout_wrapper = function( req ) {
		    return function( ) {
		        // do some logging, cleaning, etc. depending on req
		        // req.abort();
		        // callback();
		    };
		};

		var req = http.get(options, function(resp){
			if( resp.statusCode == 302 ) {// redirect (has product)
				// console.log( item.name + ' has product' );
				url = 'curl -L "http://www.target.com.au/p/' + encodeURI(item.name) + '"';
				child = exec(url, function(error, stdout, stderr){

					var $ = cheerio.load(stdout);
					itemlink = $('#hero').attr('src');

					if(itemlink){
						exec("wget 'http://www.target.com.au" + itemlink + "' -O " + item.name + ".jpg");
					}

				});
			} else {
				// console.log( item.name + ' no product' );
			}

			resp.on('data', function (data) {
		        clearTimeout( timeout );
		        timeout = setTimeout( fn, 10000 );
		    }).on('end', function () {
		        // clear timeout
		        clearTimeout( timeout );
		        callback();
		    }).on('error', function (err) {
		        // clear timeout
		        clearTimeout( timeout );
		        console.log("Got error: " + err.message);
		        callback();
		    });

		}).on("error", function(e){
		});

		// generate timeout handler
		var fn = timeout_wrapper( request );

		// set initial timeout
		var timeout = setTimeout( fn, 10000 );

	}, function(err){
		console.log(err);
		// callback();
	});

});