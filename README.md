Target AU Image Downloader
==============

Target AU image downloader uses webscraping(requests, curl and wget) compared to api to search and download target product images. The products are listed in items.json file searching using their item number/sku, checking www.target.com.au/p/[productid], then wget the hero image
--------------

*Note that this is experimental and buggy, and not all images are downloaded just based on available products set by Target AU*

**To Run**

- Require node.js, curl, wget (on windows, please check available binaries and add them to environment vars)
- run `npm install` to install deps and modules
- run `node app.js` this downloads the images in sku.jpg format based on items.json file


**Roadmap**

- Download more than one image
- Handle bugs